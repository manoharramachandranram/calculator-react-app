import React from "react";
import {
  ListItemText,
  MenuItem,
  MenuList,
  Paper,
  Typography,
} from "@mui/material";
import CalculatorUI from "./CalculatorUI";

const Main = () => {
  return (
    <div className="w-full">
      <Paper
        className="fixed left-0 top-12 bottom-0 shadow-md"
        sx={{ width: 220, maxWidth: "100%" }}
      >
        <Typography variant="h6" component="div" className="text-14 p-5 pb-0">
          Tech stack
        </Typography>
        <MenuList>
          <MenuItem className="p-2 mb-5 bg-gray-100">
            <ListItemText>React[17.0.2]</ListItemText>
          </MenuItem>
          <MenuItem className="p-2 mb-5 bg-gray-100">
            <ListItemText>TypeScript[4.4.2]</ListItemText>
          </MenuItem>
          <MenuItem className="p-2 mb-5 bg-gray-100">
            <ListItemText>MaterialUI[5.2.8]</ListItemText>
          </MenuItem>
          <MenuItem className="p-2 mb-5 bg-gray-300">
            <ListItemText>TailWindcss[3.0.13]</ListItemText>
          </MenuItem>
        </MenuList>
      </Paper>
      <div className="flex justify-center bg-gray-100 h-screen overflow-hidden">
        <CalculatorUI />
      </div>
    </div>
  );
};
export default Main;
