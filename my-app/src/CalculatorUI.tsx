import React, { useState, useEffect, useRef } from "react";
import { Button, Paper, TextField } from "@mui/material";

const numberArray = [1, 2, 3, 4, 5, 6, 7, 8, 9, 0];
const actions = [
  {
    value: "add",
    lable: "+",
  },
  {
    value: "sub",
    lable: "-",
  },
  {
    value: "mul",
    lable: "*",
  },
  {
    value: "div",
    lable: "%",
  },
  {
    value: "equal",
    lable: "=",
  },
];

export interface State {
  // input: number;
}

function usePrevious<T>(value: T): T {
  const ref: any = useRef<T>();
  useEffect(() => {
    ref.current = value;
  }, [value]);
  return ref.current;
}

const CalculatorUI = () => {
  const [input, setInput] = useState(0);
  const prevCount = usePrevious(input);
  const [result, setResult] = useState(0);

  const handleChange = (val: any) => {
    setInput(val);
  };

  const handleInputChange = (evt: any) => {
    setInput(evt.target.value);
  };

  const handleActionPerformed = (val: any) => (): void => {
    if (val === "add") {
      setResult(prevCount + input);
    }
    if (val === "sub") {
      setResult(prevCount - input);
    }
    if (val === "mul") {
      setResult(prevCount * input);
    }
    if (val === "div") {
      setResult(prevCount % input);
    }
    if (val === "equal") {
      setInput(result);
    }
  };

  return (
    <Paper elevation={3} className="w-3/12 h-fit rounded mt-10">
      <TextField
        id="outlined-basic"
        className="w-full"
        variant="outlined"
        value={input}
        onChange={handleInputChange}
      />
      <div className="flex p-5">
        <div className="w-9/12 pr-5">
          <Button variant="outlined" className="w-full">
            Clear
          </Button>
          <div className=" grid grid-cols-3 gap-3 mt-5">
            {numberArray.map((_val) => {
              return (
                <Button
                  variant="outlined"
                  className="w-full"
                  onClick={(_Val) => {
                    handleChange(_val);
                  }}
                >
                  {_val}
                </Button>
              );
            })}
          </div>
        </div>

        <div className="w-3/12">
          <div className=" grid grid-rows-3  grid-flow-row gap-5">
            {actions.map((_val) => {
              return (
                <Button
                  variant="outlined"
                  className="w-full  mt-5"
                  onClick={handleActionPerformed(_val.value)}
                >
                  {_val.lable}
                </Button>
              );
            })}
          </div>
        </div>
      </div>
    </Paper>
  );
};
export default CalculatorUI;
