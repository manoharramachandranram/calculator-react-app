import React from "react";
import "./App.css";
import Main from "./Main";

function App() {
  return (
    <div className="App">
      <header className="text-3xl font-bold shadow text-center p-2 sticky top-0 z-10 bg-white">
        Calculator App
      </header>
      <Main />
    </div>
  );
}

export default App;
